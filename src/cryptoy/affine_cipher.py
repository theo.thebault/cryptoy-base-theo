from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement affine

def compute_permutation(a: int, b: int, n: int) -> list[int]:
    return [(a * i + b) % n for i in range(n)]


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    perm = compute_permutation(a, b, n)
    return [i for i, _ in sorted(enumerate(perm), key=lambda x: x[1])]


def encrypt(msg: str, a: int, b: int) -> str:
    perm = compute_permutation(a, b, 0x110000)
    return unicodes_to_str([perm[c] for c in str_to_unicodes(msg)])


def encrypt_optimized(msg: str, a: int, b: int) -> str:
    return unicodes_to_str([(a * c + b) % 0x110000 for c in str_to_unicodes(msg)])


def decrypt(msg: str, a: int, b: int) -> str:
    perm = compute_inverse_permutation(a, b, 0x110000)
    return unicodes_to_str([perm[c] for c in str_to_unicodes(msg)])


def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    return unicodes_to_str(
        [(a_inverse * (c - b)) % 0x110000 for c in str_to_unicodes(msg)]
    )


def compute_affine_keys(n: int) -> list[int]:
    return [a for a in range(1, n) if gcd(a, n) == 1]


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    try:
        return next(a_1 for a_1 in affine_keys if a * a_1 % n == 1)
    except StopIteration:
        raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"

    for a in compute_affine_keys(0x110000):
        print("Testing a =", a)
        msg = decrypt(s, a, 58)
        if "bombe" in msg:
            return (msg, (a, 58))

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )

    affine_keys = compute_affine_keys(0x110000)
    for a in affine_keys:
        print("Testing a =", a)
        try:
            a_inv = compute_affine_key_inverse(a, affine_keys, 0x110000)
        except RuntimeError as e:
            print("Error:", e)
            continue
        for b in range(1, 10000):
            msg = decrypt_optimized(s, a_inv, b)
            if "bombe" in msg:
                return (msg, (a, b))

    raise RuntimeError("Failed to attack")
