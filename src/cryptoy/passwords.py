import hashlib
import os
from random import (
    Random,
)

import names


def hash_password(password: str) -> str:
    return hashlib.sha3_256(password.encode()).hexdigest()


def random_salt() -> str:
    return bytes.hex(os.urandom(32))


def generate_users_and_password_hashes(
    passwords: list[str], count: int = 32
) -> dict[str, str]:
    rng = Random()  # noqa: S311

    users_and_password_hashes = {
        names.get_full_name(): hash_password(rng.choice(passwords))
        for _i in range(count)
    }
    return users_and_password_hashes


def attack(passwords: list[str], passwords_database: dict[str, str]) -> dict[str, str]:
    users_and_passwords = {}

    hashed_passwords = {hash_password(password): password for password in passwords}

    for user, password_hash in passwords_database.items():
        print("Cracking :", user)
        password = hashed_passwords[password_hash]
        print("Password:", password)
        users_and_passwords[user] = password

    return users_and_passwords


def fix(
    passwords: list[str], passwords_database: dict[str, str]
) -> dict[str, dict[str, str]]:
    users_and_passwords = attack(passwords, passwords_database)

    users_and_salt = {user: random_salt() for user in users_and_passwords.keys()}
    new_database = {
        user: {
            "password_hash": hash_password(password + users_and_salt[user]),
            "password_salt": users_and_salt[user],
        }
        for user, password in users_and_passwords.items()
    }

    return new_database


def authenticate(
    user: str, password: str, new_database: dict[str, dict[str, str]]
) -> bool:
    return new_database[user]["password_hash"] == hash_password(
        password + new_database[user]["password_salt"]
    )
